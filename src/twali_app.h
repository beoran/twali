#ifndef TWALI_APP_H_INCLUDED
#define TWALI_APP_H_INCLUDED

#define TWALI_APP_MAIN_TASK 0

#include "lvgl/lvgl.h"

#ifdef __cplusplus
extern "C" {
#endif


struct twali_task;

enum twali_app_type {
    /* This app has only one screen, and is always active. E.g. a clock face. */
    twali_app_type_static = 1,
    /* This app can be started and stopped. E.g. a step counter.
     * If stopped, the app shows the start screen with the given icon. 
     * If started it shows the active interface.
     */
    twali_app_type_dynamic = 2,
};

struct twali_app {        
    const char * name;
    const char * icon_path;
    enum twali_app_type app_type;
    void * data;
    void  (*update)(struct twali_app *t);    
    struct { 
        struct twali_task * task;
        lv_obj_t * screen;
    } private;
};

int twali_app_start(struct twali_app * a, int priority);
int twali_app_stop(struct twali_app * a);

#ifdef __cplusplus
}
#endif

#endif
