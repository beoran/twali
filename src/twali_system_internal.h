#ifndef TWALI_SYSTEM_INTERNAL_H_INCLUDED
#define TWALI_SYSTEM_INTERNAL_H_INCLUDED

#ifndef TWALI_INTERNAL
#error This header file is for internal use by TWALI.
#endif

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>

#ifdef __cplusplus
#define LILYGO_TWATCH_2020_V1
#include <TTGO.h>
#endif

#include "twali_message.h"
#include "upubsub.h"
#include "twali_system.h"
#include "twali_power.h"
#include "twali_screen.h"

#ifdef __cplusplus
extern "C" {
#endif

struct twali_system {
    TTGOClass * handle;
    TFT_eSPI *tft;
    struct twali_power * power;
    struct twali_screen * screen;
    struct upubsub bus;
};

#ifdef __cplusplus
}
#endif

#endif

