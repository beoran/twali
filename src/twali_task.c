#include "twali_message.h"
#include "twali_task.h"

#include "twali_system.h"
#include "twali_power.h"

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include <stdio.h>

#define UPUBSUB_IMPLEMENTATION
#include "upubsub.h"

#ifndef TWALI_TASK_QUEUE_SIZE
#define TWALI_TASK_QUEUE_SIZE 32
#endif

#ifndef TWALI_TASK_TOPICS_COUNT
#define TWALI_TASK_TOPICS_COUNT 32
#endif



struct twali_task {
    struct twali_system * system;
    struct twali_task   * parent;
    struct twali_app    * app;
    char                * name;
    TaskHandle_t          task;
    QueueHandle_t         queue;
    void *                data;
    int                   low_power_delay;
    int                   delay;
    int                   done;
    void                  (*message)(struct twali_task *t, struct upubsub_message m);
    void                  (*update)(struct twali_task *t);
    const char          * topics[TWALI_TASK_TOPICS_COUNT];
    struct upubsub_listener * listeners[TWALI_TASK_TOPICS_COUNT];
};

struct twali_app * twali_task_app(struct twali_task * task) {
    if (!task) {
        return 0;
    }
    return task->app;
}

int twali_task_init(struct twali_task *t, 
    char * name, 
    struct twali_app * app,
    void * data, 
    void  (*message)(struct twali_task *t, struct upubsub_message m),
    void  (*update)(struct twali_task *t)
);

void twali_task_task(void * object) {
    struct twali_task * t = object;
    struct twali_power * p = twali_system_power(t->system);
    t->done = 0;
    do {
        struct upubsub_message mesg;
        /* 
        bool update_display = false;
        unsigned int delay_time = 0;
        */
        
        if( xQueueReceive(t->queue,
                         &(mesg),
                         ( TickType_t ) 10 ) == pdPASS ) {
            if (t->message) {
                t->message(t, mesg);
            } else {
                printf("Task %s received message topic %s\n", t->name, mesg.topic);
            }
        }

        if (twali_power_low(p) && (t->low_power_delay > 0)) {
            vTaskDelay(t->low_power_delay / portTICK_PERIOD_MS);
            continue;
        }

        /* twali_task_check_wake_time(t); */
        if (t->update) {
            t->update(t);
        }    
        vTaskDelay(t->delay / portTICK_PERIOD_MS);
  } while (!t->done);
  twali_task_stop(t);
}

int task_upubsub_listen_func(struct upubsub_listener *l, struct upubsub_message m) {
    struct twali_task *t = l->data;
    return (int)xQueueSend(t->queue, &m, 0);
}

struct upubsub_listener * twali_task_subscribe_topic(struct twali_task * t, const char * topic) {
    return twali_system_subscribe_listener(t->system, topic, t, task_upubsub_listen_func);
}

int twali_task_subscribe_all_topics(struct twali_task *t) {
    int i;
    if (!t->system) {
        return 0;
    }
    for (i = 0; i < TWALI_TASK_TOPICS_COUNT; i++) {
        const char * topic = t->topics[i];
        if (topic) {
            t->listeners[i] = twali_task_subscribe_topic(t, topic);
        } else {
            t->listeners[i] = NULL;
        }
    }
    return 0;
}

int twali_task_start(struct twali_task * t, int priority) {
    BaseType_t res;
    res = xTaskCreate(twali_task_task, t->name, 10000, (void *) t, priority, &t->task);
    t->queue = xQueueCreate( (UBaseType_t) TWALI_TASK_QUEUE_SIZE,
                             (UBaseType_t) sizeof(struct upubsub_message));
    /*if (!t->system) {
        t->system = twali_system_singleton_pointer;
    }*/
    if (!t->system) {
        return 0;
    }
    twali_task_subscribe_all_topics(t);
    return res == pdPASS && t->queue != 0;
}

int twali_task_stop(struct twali_task * t) {
    printf("deleting task %s\n", t->name);
    int i;
    for (i = 0; i < TWALI_TASK_TOPICS_COUNT; i ++) {
        if (t->listeners[i]) {
            upubsub_unsubscribe(t->listeners[i]);
        }
    }
    vQueueDelete(t->queue);
    vTaskDelete(NULL);
    return 0;
}

/*
class Actor {
  public:
    static TTGOClass *watch;
    static TFT_eSPI *screen;
    static PowerManager * power;
    static TaskHandle_t display_task;

    // system accessors
    static void setPower(PowerManager * _power);
    static void setDisplayTask(TaskHandle_t _display_task);
    static void setWatch(TTGOClass * _watch);

    // setup and init
    bool needsInit();
    void setup();
    virtual void init() { }

    // run and display
    virtual void run() = 0;
    void execute(unsigned int & sleep_time, bool & display_update);
    virtual void display() = 0;
    virtual const uint32_t displayIdentifier() = 0;

    // low power considerations
    virtual const bool runDuringLowPower() { return false; }
    virtual const uint32_t delayDuringLowPower() { return 1000; }

    // sleep-wake
    static void systemWokeUp();
    void checkWakeTime();
    bool wakeUpRun();

  protected:
    unsigned int delay_time = 100;
    bool refresh_display = false;

  private:
    static unsigned int woke_up_at;
    unsigned int last_wake_up_tasked = 0;
    bool wake_up_run = true;
    bool inited = false;
};

void actorTask(void * object);
void runActor(const char * name, Actor * object, int priority);

*/
