#ifndef TWALI_MESSAHE_H_INCLUDED
#define TWALI_MESSAHE_H_INCLUDED


enum twali_message_type {
    twali_message_type_none  = 0,
    twali_message_type_power = 1,
    twali_message_type_touch = 2,
    twali_message_type_text  = 3
};

enum twali_power_message_state {
    twali_power_message_state_up    = 1,
    twali_power_message_state_low   = 2,
    twali_power_message_state_sleep = 3,
    twali_power_message_state_off   = 4,
};

struct twali_power_message {
    enum twali_power_message_state state;
};

struct twali_touch_message {
    int x;
    int y;
};

#define TWALI_TEXT_MESSAGE_SIZE 64
struct twali_text_message {
        char bytes[TWALI_TEXT_MESSAGE_SIZE];
};

struct twali_message {
    enum twali_message_type message_type;
    union {
        struct twali_power_message power;
        struct twali_touch_message touch;
        struct twali_text_message text;
    } body;
};

#define UPUBSUB_MESSAGE_TYPE struct twali_message

#endif
