#ifndef TWALI_TASK_H_INCLUDED
#define TWALI_TASK_H_INCLUDED

struct twali_task;
struct twali_message;
struct twali_app;

int twali_task_send(struct twali_task * from, 
struct twali_task * to, 
struct twali_message * msg);

int twali_task_start(struct twali_task * t, int priority);
int twali_task_stop(struct twali_task * t);

struct twali_app * twali_task_app(struct twali_task * task);

#endif
