#include "twali_app.h"
#include "twali_task.h"


#include "lvgl/lvgl.h"

#ifdef __cplusplus
extern "C" {
#endif


void twali_app_update_task(struct twali_task * task) {
    struct twali_app *app = twali_task_app(task);
    if (task && app>app) {
        app->update(app);
    }
}

int twali_app_start(struct twali_app * a, int priority) {
    twali_task_start(a->private.task, priority);
}

int twali_app_stop(struct twali_app * a) {
    twali_task_stop(a->private.task);
    a->private.task = 0;    
}

#ifdef __cplusplus
}
#endif


