#ifndef TWALI_SYSTEM_H_INCLUDED
#define TWALI_SYSTEM_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#include "twali_message.h"
#include "upubsub.h"

struct twali_system;
struct twali_power;
struct twali_task;

struct twali_power * twali_system_power(struct twali_system * s);

void twali_system_power_interrupt(void);
struct twali_system * twali_system_make(void);
void twali_system_start(struct twali_system * s);
void twali_system_update(struct twali_system * s);

void twali_system_wake_up(struct twali_system *s);

int twali_system_publish_data(struct twali_system *s, const char * topic, void * mesg, size_t size);
int twali_system_publish_str(struct twali_system *s, const char * topic, char * str);
int twali_system_publish_message(struct twali_system *s, const char * topic, struct twali_message m);
int twali_system_publish_text(struct twali_system *s, const char * topic, const char * text);
int twali_system_publish_touch(struct twali_system *s, const char * topic, int x, int y);
int twali_system_publish_power(struct twali_system *s, const char * topic, enum twali_power_message_state state);

struct upubsub_listener * twali_system_subscribe_listener(struct twali_system *s, 
const char * topic, void * data, upubsub_listen_func * listen);
struct upubsub_listener * twali_system_subscribe_task(struct twali_system *s, 
const char * topic, void * data, struct twali_task * task);
int twali_system_unsubscribe_listener(struct twali_system *s, 
const char * topic, struct upubsub_listener * listener);
struct upubsub_listener * twali_system_unsubscribe_task(struct twali_system *s, 
const char * topic,  struct twali_task * task);




#ifdef __cplusplus
}
#endif

#endif
