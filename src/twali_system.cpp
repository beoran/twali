
#define LILYGO_TWATCH_2020_V1
#include <TTGO.h>

#define TWALI_INTERNAL
#include "twali_system_internal.h"
#include "twali_power.h"
#include "twali_screen.h"


extern "C" {

#include <string.h>

static struct twali_system twali_system_singleton;
static struct twali_system * twali_system_singleton_pointer = NULL;

struct twali_power * twali_system_power(struct twali_system * s) {
    return s->power;
}


void twali_system_power_interrupt(void) {
    if (twali_system_singleton_pointer) {
        twali_power_interrupt(twali_system_singleton_pointer->power);
    }
} 

struct twali_system * twali_system_make(void) {
    if (twali_system_singleton_pointer) {
        return twali_system_singleton_pointer;
    }    

    twali_system_singleton.handle = TTGOClass::getWatch();
    twali_system_singleton.handle->begin();
    twali_system_singleton.handle->rtc->check();
    twali_system_singleton.handle->bl->adjust(150);
    twali_system_singleton.tft = twali_system_singleton.handle->eTFT;
    twali_system_singleton.tft->fillScreen(TFT_BLUE);
    twali_system_singleton.tft->setTextColor(TFT_WHITE, TFT_BLACK);
    twali_system_singleton.tft->setTextFont(8);
    twali_system_singleton.handle->openBL();  
    twali_system_singleton.power  = twali_power_make(&twali_system_singleton);
    twali_system_singleton.bus = { 0 };
    pinMode(AXP202_INT, INPUT);
    attachInterrupt(AXP202_INT, twali_system_power_interrupt, FALLING);

    twali_system_singleton_pointer = &twali_system_singleton;
    return twali_system_singleton_pointer;
}

void twali_system_start(struct twali_system * s) {
    twali_power_start_task(s->power);
}

void twali_system_update(struct twali_system * s) {
    vTaskDelay(10000);
}

void twali_system_wake_up(struct twali_system *s) {
    twali_system_publish_power(s, "POWER", twali_power_message_state_up);
}

int twali_system_publish_data(struct twali_system *s, const char * topic, void * mesg, size_t size) {
    return upubsub_publish_data(s->bus, topic, mesg, size);
}

int twali_system_publish_str(struct twali_system *s, const char * topic, char * str) {
    return upubsub_publish_str(s->bus, topic, str);
}

int twali_system_publish_message(struct twali_system *s, const char * topic, struct twali_message msg) {
    return upubsub_publish_type(s->bus, topic, msg);
}

int twali_system_publish_text(struct twali_system *s, const char * topic, const char * text) {
    struct twali_message msg = { twali_message_type_none };
    msg.message_type = twali_message_type_text;
    size_t len = strlen(text);
    /* truncate text */
    if (len >= sizeof(msg.body.text.bytes) - 1) {
        len = sizeof(msg.body.text.bytes) - 1;
    }
    strncpy(msg.body.text.bytes, text, len);
    msg.body.text.bytes[len] = '\0';
    return twali_system_publish_message(s, topic, msg);
}

int twali_system_publish_touch(struct twali_system *s, const char * topic, int x, int y) {
    struct twali_message msg = { twali_message_type_none };
    msg.message_type = twali_message_type_touch;
    msg.body.touch.x = x;
    msg.body.touch.y = y;
    return twali_system_publish_message(s, topic, msg);
}

int twali_system_publish_power(struct twali_system *s, const char * topic, enum twali_power_message_state state) {
    struct twali_message msg = { twali_message_type_none };
    msg.message_type = twali_message_type_power;
    msg.body.power.state = state;
    return twali_system_publish_message(s, topic, msg);
}


}
