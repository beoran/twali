#ifndef TWALI_SYSTEM_H_INCLUDED
#define TWALI_SYSTEM_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

struct twali_screen;

struct twali_screen *
twali_screen_sub(struct twali_screen s, x int, y int, w int, h int);


#ifdef __cplusplus
}
#endif

#endif

