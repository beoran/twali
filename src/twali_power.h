#ifndef TWALI_POWER_H_INCLUDED
#define TWALI_POWER_H_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define TWALI_POWER_TOPIC "POWER"

struct twali_power;
struct twali_task;

struct twali_power * twali_power_make(struct twali_system *s);
void twali_power_init(struct twali_power *p);
void twali_power_interrupt(struct twali_power *p);
void twali_power_run(struct twali_power *p);
int twali_power_plugged_in(struct twali_power *p);
int twali_power_charging(struct twali_power *p);
int twali_power_low(struct twali_power *p);
int twali_power_battery_percentage(struct twali_power *p);
float twali_power_battery_current(struct twali_power *p);
float twali_power_vbus_current(struct twali_power *p);
float twali_power_last_interaction(struct twali_power *p);
float twali_power_now(struct twali_power *p);
void twali_power_up(struct twali_power *p);
void twali_power_down(struct twali_power *p);
void twali_power_suspend(struct twali_power *p);

void twali_power_task(void* object);
void twali_power_start_task(struct twali_power * p);

#ifdef __cplusplus
}
#endif


#endif
